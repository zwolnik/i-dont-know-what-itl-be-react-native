import { Body, Engine, World } from "matter-js";

type State = {
  physics: { engine: Engine; world: World };
  bird: { body: Body; renderer: object };
  floor: { body: Body; renderer: object };
};

export { State };
