import React from "react";
import { Image, View } from "react-native";
import { Body } from "matter-js";

interface EntityProps {
  body: Body;
}

const bodyToXywh = (
  body: Body
): { x: number; y: number; h: number; w: number } => {
  const w = Math.abs(body.bounds.max.x - body.bounds.min.x);
  const h = Math.abs(body.bounds.max.y - body.bounds.min.y);
  const x = body.position.x;
  const y = body.position.y;
  return { x, y, w, h };
};

const Bird = ({ body }: EntityProps) => {
  const { x, y, h, w } = bodyToXywh(body);

  return (
    <Image
      style={{
        width: w,
        height: h,
        top: y - h / 2,
        left: x - w / 2,
        position: "absolute",
        backgroundColor: "transparent",
      }}
      resizeMode="contain"
      // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
      source={require("./assets/appa.png")}
    />
  );
};

const Floor = ({ body }: EntityProps) => {
  const { x, y, h, w } = bodyToXywh(body);

  return (
    <Image
      style={{
        width: w,
        height: h,
        top: y - h / 2,
        left: x - w / 2,
        position: "absolute",
        backgroundColor: "transparent",
      }}
      resizeMode="repeat"
      // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
      source={require("./assets/grass-cliff-mid.png")}
    />
  );
};

export { Bird, Floor };
