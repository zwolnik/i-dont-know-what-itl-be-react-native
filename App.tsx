import React, { useRef, useState, useEffect } from "react";
import { StyleSheet, StatusBar, View, useWindowDimensions } from "react-native";
import { Bodies, Engine, World } from "matter-js";
import { GameEngine } from "react-native-game-engine";

import { State } from "./State";
import { Physics } from "./Systems";
import { Bird, Floor } from "./Renderers";

const App = () => {
  const { width, height } = useWindowDimensions();
  const engine = useRef<Engine>(Engine.create());
  const [running, setRunning] = useState<boolean>(false);
  const state = useRef<undefined | State>(undefined);

  useEffect(() => {
    engine.current.gravity.y = 0.5;
    const bird = Bodies.rectangle(width / 2, height / 2, 100, 75);
    const floor = Bodies.rectangle(width / 2, height - 25, width, 50, {
      isStatic: true,
    });
    World.add(engine.current.world, [bird, floor]);
    state.current = {
      physics: { engine: engine.current, world: engine.current.world },
      bird: { body: bird, renderer: Bird },
      floor: { body: floor, renderer: Floor },
    };
    setRunning(true);

    return () => {
      if (state.current) {
        World.clear(state.current.physics.world, false);
        Engine.clear(state.current.physics.engine);
      }
      setRunning(false);
      state.current = undefined;
    };
  }, []);

  return (
    <View style={styles.container}>
      {state.current && (
        <GameEngine
          style={styles.game}
          systems={[Physics]}
          running={running}
          entities={state.current}
        >
          <StatusBar hidden={true} />
        </GameEngine>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  game: {
    position: "absolute",
    top: 0,
    left: 0,
    width: "100%",
    height: "100%",
    backgroundColor: "#212121",
  },
});

export default App;
