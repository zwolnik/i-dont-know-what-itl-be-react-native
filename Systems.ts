import Matter from "matter-js";
import { State } from "./State";
import { GameEngineSystem } from "react-native-game-engine";

const Physics: GameEngineSystem = (state: State, { touches, time }) => {
  const bird = state.bird.body;
  touches
    .filter((t) => t.type === "press")
    .forEach(() => {
      Matter.Body.setVelocity(bird, {
        ...bird.velocity,
        y: bird.velocity.y - 10,
      });
    });

  const engine = state.physics.engine;
  Matter.Engine.update(engine, time.delta);
  return state;
};

export { Physics };
